package com.example.eecs481hw2;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    public void changeText(View view) {
    	TextView t = (TextView)findViewById(R.id.textView1);
    	EditText e = (EditText)findViewById(R.id.editText1);
    	
    	t.setText("Hello " + e.getText());
    }
    
}
